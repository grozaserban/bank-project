namespace Bank.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class TransferChange1 : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Transfers",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        SourceID = c.Int(),
                        TargetID = c.Int(),
                        Value = c.Decimal(nullable: false, precision: 18, scale: 2),
                        Date = c.DateTime(nullable: false),
                        Details = c.String(),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.Accounts", t => t.SourceID)
                .ForeignKey("dbo.Accounts", t => t.TargetID)
                .Index(t => t.SourceID)
                .Index(t => t.TargetID);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Transfers", "TargetID", "dbo.Accounts");
            DropForeignKey("dbo.Transfers", "SourceID", "dbo.Accounts");
            DropIndex("dbo.Transfers", new[] { "TargetID" });
            DropIndex("dbo.Transfers", new[] { "SourceID" });
            DropTable("dbo.Transfers");
        }
    }
}
