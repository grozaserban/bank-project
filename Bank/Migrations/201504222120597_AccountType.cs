namespace Bank.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AccountType : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.Accounts", "Type", c => c.Int(nullable: false));
        }
        
        public override void Down()
        {
            AlterColumn("dbo.Accounts", "Type", c => c.String(nullable: false));
        }
    }
}
