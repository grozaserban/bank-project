﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;

namespace Bank.Models
{
    public enum AccountTypes
    {
        Deposit = 1,
        Loan = 2,
        Savings = 3
    }

    public class Account
    {
        public int ID { get; set; }

        [Required()]
        public AccountTypes Type { get; set; }


        public decimal Balance { get; set; }

        [Required()]
        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:yyyy-MM-dd}", ApplyFormatInEditMode = true)]
        public DateTime CreationDate { get; set; }

       
        [ForeignKey("Client")]
        public virtual int? ClientID { get; set; }
        public virtual Client Client { get; set; }
    }
}
