﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Bank.Models
{
    public class Client
    {
        public int ID { get; set; }

        [Required()]
        public string Name { get; set; }


        public string IDCardNumber { get; set; }

        public string PNC { get; set; }

        public string Address { get; set; }


        public virtual ICollection<Account> Accounts { get; set; }
    }
}