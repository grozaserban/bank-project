﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Bank.Models;

namespace Bank.Controllers
{
    [Authorize]
    public class TransfersController : Controller
    {
        private BankContext db = new BankContext();

        // GET: Transfers
        public ActionResult Index()
        {
            var transfers = db.Transfers.Include(t => t.Source).Include(t => t.Target);
            return View(transfers.ToList());
        }

        // GET: Transfers/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Transfer transfer = db.Transfers.Find(id);
            if (transfer == null)
            {
                return HttpNotFound();
            }
            return View(transfer);
        }

        // GET: Transfers/Create
        public ActionResult Create()
        {
            ViewBag.SourceID = new SelectList(db.Accounts, "ID", "ID");
            ViewBag.TargetID = new SelectList(db.Accounts, "ID", "ID");
            return View();
        }

        // POST: Transfers/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "ID,SourceID,TargetID,Value,Date,Details")] Transfer transfer)
        {
            if (ModelState.IsValid)
            {
               
                Account source=db.Accounts.Find(transfer.SourceID);
                Account target = db.Accounts.Find(transfer.TargetID);
                source.Balance -= transfer.Value;
                target.Balance += transfer.Value;

                db.Transfers.Add(transfer);
                db.Entry(source).State = EntityState.Modified;
                db.Entry(target).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.SourceID = new SelectList(db.Accounts, "ID", "ID", transfer.SourceID);
            ViewBag.TargetID = new SelectList(db.Accounts, "ID", "ID", transfer.TargetID);
            return View(transfer);
        }

        // GET: Transfers/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Transfer transfer = db.Transfers.Find(id);
            if (transfer == null)
            {
                return HttpNotFound();
            }
            ViewBag.SourceID = new SelectList(db.Accounts, "ID", "ID", transfer.SourceID);
            ViewBag.TargetID = new SelectList(db.Accounts, "ID", "ID", transfer.TargetID);
            return View(transfer);
        }

        // POST: Transfers/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "ID,SourceID,TargetID,Value,Date,Details")] Transfer transfer)
        {
            if (ModelState.IsValid)
            {
                db.Entry(transfer).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.SourceID = new SelectList(db.Accounts, "ID", "ID", transfer.SourceID);
            ViewBag.TargetID = new SelectList(db.Accounts, "ID", "ID", transfer.TargetID);
            return View(transfer);
        }

        // GET: Transfers/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Transfer transfer = db.Transfers.Find(id);
            if (transfer == null)
            {
                return HttpNotFound();
            }
            return View(transfer);
        }

        // POST: Transfers/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Transfer transfer = db.Transfers.Find(id);
            db.Transfers.Remove(transfer);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
